﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication2
{
    [ApiController]
    [Route("Sum")]
    public class Calculator : ControllerBase
    {
        [HttpPost (Name = "Sum")]
        public int Sum(int num1, int num2) 
        {
            return num1 + num2;
        }
    }
}
